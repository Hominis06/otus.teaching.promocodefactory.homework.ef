﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;

namespace Otus.Teaching.PromoCodeFactory.DataAccess
{
    public class EFDbInitializer : IEFDbInitializer
    {
        private readonly EFDataContext _efDataContext;

        public EFDbInitializer(EFDataContext dataContext)
        {
            _efDataContext = dataContext;
        }

        public void Initialize()
        {
            _efDataContext.Database.Migrate();
        }
    }
}
