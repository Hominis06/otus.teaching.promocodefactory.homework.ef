﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class EfRepository<T> : IRepository<T> where T : BaseEntity
    {
        private readonly EFDataContext _dataContext;

        public EfRepository(EFDataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public async Task<bool> CreateAsync(T entity) => await _dataContext.Set<T>().AddAsync(entity).Result.Context.SaveChangesAsync() > 0;

        public async Task<bool> DeleteAsync(Guid id)
        {
            var entity = await GetByIdAsync(id);
            return entity != null && await _dataContext.Set<T>().Remove(entity).Context.SaveChangesAsync() > 0;
        }

        public async Task<IEnumerable<T>> GetAllAsync() => await _dataContext.Set<T>().ToListAsync();

        public async Task<T> GetByIdAsync(Guid id) => await _dataContext.Set<T>().FirstOrDefaultAsync(x => x.Id == id);

        public async Task<bool> UpdateAsync(T entity) => await _dataContext.Set<T>().Update(entity).Context.SaveChangesAsync() > 0;
    }
}
