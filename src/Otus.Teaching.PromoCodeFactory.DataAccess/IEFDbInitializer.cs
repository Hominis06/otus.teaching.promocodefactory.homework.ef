﻿
namespace Otus.Teaching.PromoCodeFactory.DataAccess
{
    public interface IEFDbInitializer
    {
        void Initialize();
    }
}
