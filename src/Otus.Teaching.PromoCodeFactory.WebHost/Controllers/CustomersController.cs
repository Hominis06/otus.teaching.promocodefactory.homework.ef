﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Клиенты
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CustomersController
        : ControllerBase
    {
        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<Preference> _preferenceRepository;
        private readonly IRepository<PromoCode> _promoCodeRepository;
        private readonly IMapper _mapper;

        public CustomersController(IRepository<Customer> customerRepository,
                                   IRepository<Preference> preferenceRepository,
                                   IRepository<PromoCode> promoCodeRepository,
                                   IMapper mapper)
        {
            _customerRepository = customerRepository;
            _preferenceRepository = preferenceRepository;
            _promoCodeRepository = promoCodeRepository;
            _mapper = mapper;
        }

        /// <summary>
        /// Получить список клиентов
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IEnumerable<CustomerShortResponse>> GetCustomersAsync() => _mapper.Map<IEnumerable<CustomerShortResponse>>(await _customerRepository.GetAllAsync());

        /// <summary>
        /// Получить клиента по Id
        /// </summary>
        /// <param name="id">Id клиента</param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<CustomerResponse>> GetCustomerAsync(Guid id) => _mapper.Map<CustomerResponse>(await _customerRepository.GetByIdAsync(id));

        /// <summary>
        /// Создать клиента
        /// </summary>
        /// <param name="request">Данные о клиенте</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> CreateCustomerAsync(CreateOrEditCustomerRequest request)
        {
            try
            {
                Customer customer = _mapper.Map<Customer>(request);
                request.PreferenceIds.ForEach(async pId => customer.Preferences.Add(await _preferenceRepository.GetByIdAsync(pId)));
                if(await _customerRepository.CreateAsync(customer))
                {
                    return Ok();
                }
                else
                {
                    return BadRequest();
                }
            }
            catch
            {
                return BadRequest();
            }
        }

        /// <summary>
        /// Редактировать клиента
        /// </summary>
        /// <param name="id">Id клинета для изменения</param>
        /// <param name="request">Новые данные клиента</param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> EditCustomersAsync(Guid id, CreateOrEditCustomerRequest request)
        {
            List<Preference> preferences;
            try
            {
                Customer dbCustomer = await _customerRepository.GetByIdAsync(id);
                if (dbCustomer == null)
                {
                    return NotFound();
                }

                dbCustomer.FirstName = request.FirstName;
                dbCustomer.LastName = request.LastName;
                dbCustomer.Email = request.Email;
                preferences = dbCustomer.Preferences ?? new List<Preference>();
                preferences = preferences.Where(p => request.PreferenceIds.Contains(p.Id))
                                         .ToList();

                request.PreferenceIds.Except(preferences.Select(p => p.Id))
                                     .ToList()
                                     .ForEach(async pId => preferences.Add(await _preferenceRepository.GetByIdAsync(pId)));

                dbCustomer.Preferences = preferences;
                 
                if (await _customerRepository.UpdateAsync(dbCustomer))
                {
                    return Ok();
                }
                else
                {
                    return BadRequest();
                }
            }
            catch
            {
                return BadRequest();
            }
        }

        /// <summary>
        /// Удалить клиента
        /// </summary>
        /// <param name="id">Id клиента для удаления</param>
        /// <returns></returns>
        [HttpDelete]
        public async Task<IActionResult> DeleteCustomer(Guid id)
        {
            try
            {
                Customer customer = await _customerRepository.GetByIdAsync(id);

                customer.PromoCodes.Select(p => p.Id)
                                   .ToList()
                                   .ForEach(async p => await _promoCodeRepository.DeleteAsync(p));

                if (await _customerRepository.DeleteAsync(customer.Id))
                {
                    return Ok();
                }
                else
                {
                    return BadRequest();
                }
            }
            catch
            {
                return BadRequest();
            }

        }
    }
}