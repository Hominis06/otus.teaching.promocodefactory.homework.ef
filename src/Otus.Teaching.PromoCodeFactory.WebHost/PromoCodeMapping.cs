﻿using AutoMapper;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System.Collections.Generic;

namespace Otus.Teaching.PromoCodeFactory.WebHost
{
    public class PromoCodeMapping : Profile
    {
        public PromoCodeMapping()
        {
            #region customer
            CreateMap<Customer, CustomerShortResponse>();
            CreateMap<Customer, CustomerResponse>();

            CreateMap<CreateOrEditCustomerRequest, Customer>().AfterMap((createOrEdit, customer) =>
            {
                customer.Preferences ??= new List<Preference>();
            });
            #endregion
            CreateMap<PromoCode, PromoCodeShortResponse>();
            CreateMap<Preference, PreferenceResponse>();

            CreateMap<Employee, EmployeeShortResponse>();
            CreateMap<Employee, EmployeeResponse>();
            CreateMap<Role, RoleItemResponse>();
        }
    }
}
